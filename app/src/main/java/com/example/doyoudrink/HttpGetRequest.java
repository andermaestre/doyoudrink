package com.example.doyoudrink;

import android.os.AsyncTask;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpGetRequest extends AsyncTask<String, Void, String> {
    public static final String REQUEST_METHOD="GET";
    public static final int READ_TIMEOUT=15000;
    public static final int CONNECTION_TIMEOUT=15000;
    public TextView t;

    public HttpGetRequest(TextView t) {
        this.t = t;
    }

    @Override
    protected String doInBackground(String... params) {
        String stringURL=params[0];
        String result;
        String inputLine;
        try{
            URL myURL=new URL(stringURL);
            HttpURLConnection connection=(HttpURLConnection)myURL.openConnection();

            connection.setRequestMethod(REQUEST_METHOD);
            connection.setReadTimeout(READ_TIMEOUT);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);
            connection.connect();

            InputStreamReader sr = new InputStreamReader(connection.getInputStream());

            BufferedReader reader  = new BufferedReader(sr);
            StringBuilder sb = new StringBuilder();

            while((inputLine=reader.readLine()) != null){
                sb.append(inputLine);
            }

            sr.close();
            reader.close();

            result=sb.toString();

        }catch(Exception e){
            result = e.getMessage();
        }


        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        t.setText(s);
    }
}
