package com.example.doyoudrink;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    TextView txtRes;
    public static String apiBebidas="https://www.thecocktaildb.com/api/json/v1/1/search.php?i=vodka";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String result="0";
        txtRes=findViewById(R.id.lblRes);
        HttpGetRequest request = new HttpGetRequest(txtRes);
        try {
            result=request.execute(apiBebidas).get();

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
       // txtRes.setText(result);

    }
}
